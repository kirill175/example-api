<?php

namespace App\Api\v1;

use App\Helpers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Symfony\Component\HttpFoundation\Response;
use App\Api\v1\Exceptions\ApiWrongFieldNameException;
use App\Api\v1\Exceptions\ApiWrongFieldValueException;

class ApiController extends Controller
{
    protected $options = [];

    /**
     * Interface to output data in different formats.
     *
     * @var ApiOutputInterface
     */
    private $outputter;

    public function __construct(Request $request)
    {
        parent::__construct();

        if ($request->has('fields')) {
            $this->options['fields'] = explode(',', $request->input('fields'));
        }

        if ($request->has('extra_fields')) {
            $this->options['extra_fields'] = explode(',', $request->input('extra_fields'));
        }

        $this->outputter = $request->input('pretty', false) ? new ApiPrettyOutput() : new ApiJsonOutput();
    }

    /**
     * Process exception.
     *
     * @param  \Exception $e
     * @return \Response
     */
    public function processException(\Exception $e)
    {
        if ($e instanceof ApiWrongFieldNameException) {
            return $this->respondError(Response::HTTP_NOT_FOUND, 'Wrong field name', $e->getMessage());
        }

        if ($e instanceof ApiWrongFieldValueException) {
            return $this->respondError(Response::HTTP_NOT_FOUND, 'Wrong field value', $e->getMessage());
        }

        Helpers::logException($e);
        return $this->respondError(Response::HTTP_INTERNAL_SERVER_ERROR, 'Server error', 'Server error');
    }

    /**
     * Respond with error.
     *
     * @param  string $status
     * @param  string $title
     * @param  string $detail
     * @return \Response
     */
    public function respondError($status, $title, $detail)
    {
        $errors = [
            'title' => $title,
            'detail' => $detail,
        ];

        return $this->respond([], [], [$errors]);
    }

    /**
     * Respond with data.
     *
     * @param  array $data
     * @param  array $meta
     * @param  array $errors
     * @param  array $headers
     * @return \Response
     */
    public function respond(array $data = [], array $meta = [], array $errors = [], array $headers = [])
    {
        $out = [];

        if (!empty($errors)) {
            $out['errors'] = $errors;
        } else {
            $out['data'] = $data;
            if (!empty($meta)) {
                $out['meta'] = $meta;
            }
        }

        return $this->outputter->output($out, $headers);
    }
}
