<?php

namespace App\Api\v1;

class ApiPrettyOutput implements ApiOutputInterface
{
    /**
     * {@inheritdoc}
     */
    public function output(array $outputData, array $headers = [])
    {
        return view('api.pretty', compact('outputData'));
    }
}
