<?php

namespace App\Api\v1\Exceptions;

class ApiWrongFieldValueException extends \Exception
{
    private $field_name;

    public function __construct($field_name)
    {
        \Exception::__construct('Wrong field value: ' . $field_name);
        $this->field_name = $field_name;
    }

    public function getFieldName()
    {
        return $this->field_name;
    }
}