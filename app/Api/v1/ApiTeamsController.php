<?php

namespace App\Api\v1;

use App\Team;
use Illuminate\Http\Request;
use App\Api\v1\Transformers\TeamTransformer;

class ApiTeamsController extends ApiController
{
    /**
     * /api/v1/teams
     *
     * @param Request $request
     * @return \Response
     */
    public function index(Request $request)
    {
        try {
            if ($request->has('team_id')) {
                $teamsIds = explode(',', $request->input('team_id', ''));
                $data = [];
                foreach ($teamsIds as $teamId) {
                    $team = Team::find($teamId);
                    if ($team) {
                        $data[] = TeamTransformer::transform($team, $this->options);
                    }
                }
                return $this->respond($data);
            }

            if ($request->has('parse')) {
                $data = Team::parse($request->get('parse'))
                    ->get()
                    ->map(function (Team $team) {
                        return TeamTransformer::transform($team, $this->options);
                    })
                    ->toArray();
                return $this->respond($data);
            }

            $query = mb_strtolower($request->input('q', ''));
            if (mb_strlen($query) < 2) {
                return $this->respond();
            }

            $data = Team::where('search_string', 'LIKE', '%'.$query.'%')
                ->orderBy('team_name', 'ASC')
                ->get()
                ->map(function (Team $team) {
                    return TeamTransformer::transform($team, $this->options);
                })
                ->toArray();

            return $this->respond($data);
        } catch (\Exception $e) {
            return $this->processException($e);
        }
    }

    /**
     * /api/v1/teams/{id}
     *
     * @param $id
     * @return \Response
     */
    public function show($id)
    {
        try {
            $team = Team::find($id);

            if (!$team) {
                return $this->respondError(404, 'Not found', 'Team id='.$id.' not found');
            }

            $data = TeamTransformer::transform($team, $this->options);
            return $this->respond($data);
        } catch (\Exception $e) {
            return $this->processException($e);
        }
    }
}