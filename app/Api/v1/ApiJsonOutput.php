<?php

namespace App\Api\v1;

class ApiJsonOutput implements ApiOutputInterface
{
    /**
     * {@inheritdoc}
     */
    public function output(array $outputData, array $headers = [])
    {
        return \Response::json($outputData, 200, $headers);
    }
}
