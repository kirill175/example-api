<?php

namespace App\Api\v1\Transformers;

use App\Team;
use App\Api\v1\Exceptions\ApiWrongFieldNameException;

class TeamTransformer extends ModelTransformer
{
    protected static $allowed_fields = [
        'id',
        'slug',
        'url',
        'name',
        'name_eng',
        'city',
        'country',
        'search_string',
        'search_string_just_name',
    ];

    public static function transform(Team $team, $options = [])
    {
        $city = $team->getCity();
        $country = $team->getCountry();
        $result = [
            'id' => (int)$team->id,
            'slug' => $team->slug,
            'url' => $team->path(),
            'name' => $team->team_name,
            'name_eng' => $team->team_name_eng,
            'city' => $city ? CityTransformer::transform($city) : null,
            'country' => $country ? CountryTransformer::transform($country) : null,
            'location' => $team->getLocationString(),
            'logo' => $team->getLogo(),
        ];

        if (!empty($options['fields'])) {
            $result = static::filterFields($result, $options['fields']);
        }

        if (!empty($options['extra_fields'])) {
            foreach ($options['extra_fields'] as $field_name) {
                if (!\in_array($field_name, static::$allowed_fields)) {
                    throw new ApiWrongFieldNameException($field_name);
                }
                switch ($field_name) {
                    case 'search_string':
                        $result['search_string'] = $team->search_string;
                        break;
                    case 'search_string_name':
                        $result['search_string_name'] = $team->search_string_just_name;
                        break;

                    default:
                        throw new ApiWrongFieldNameException($field_name);
                        break;
                }
            }
        }

        return $result;
    }
}
