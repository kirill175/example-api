<?php

namespace App\Api\v1\Transformers;

use App\Api\v1\Exceptions\ApiWrongFieldNameException;

abstract class ModelTransformer
{
    protected static $allowed_fields = [];

    /**
     * Keep only specific fields in result data.
     *
     * @param  array $data
     * @param  array $fields
     * @return array
     * @throws ApiWrongFieldNameException
     */
    public static function filterFields(array $data, array $fields)
    {
        $result = [];

        if (empty($fields)) {
            return $data;
        }

        foreach ($fields as $field_name) {
            if (!\in_array($field_name, static::$allowed_fields)) {
                throw new ApiWrongFieldNameException($field_name);
            }
            if (isset($data[$field_name])) {
                $result[$field_name] = $data[$field_name];
            }
        }

        return $result;
    }
}