<?php

namespace App\Api\v1;

interface ApiOutputInterface
{
    /**
     * @param array $outputData
     * @param array $headers
     * @return \Response
     */
    public function output(array $outputData, array $headers = []);
}
